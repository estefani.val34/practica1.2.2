package practica_1.pkg2.pkg2;

import java.io.IOException;
import java.util.Date;
import java.util.Scanner;
/**
 * 
 * @author tarda
 * 
 * 
 */
public class Practica_122 {

    static void menu() {
        System.out.println("1.-Donar la volta\n"
                + "2.- Trobar la base més repetida,\n"
                + "3.- Trobar la base menys repetida\n"
                + "4.- Fer recompte de bases\n"
                + "5.- Sortir.");
    }

    public static void main(String[] args) throws IOException {

        Scanner teclat = new Scanner(System.in);

        int option;

        String ADN;

        String ruta = "/home/tarda/practica1.2.2/src/practica_1/pkg2/pkg2/";
        String nameFile = "archivo.txt";
        ADN_FileReader file = new ADN_FileReader();

        ADN = file.muestraContenido(ruta + nameFile);

        do {
            menu();
            System.out.println("Enter a option:");
            option = teclat.nextInt();

            switch (option) {
                case 1:
                    System.out.println("**Donar la volta a:"+ADN+"**");
                    System.out.println("");
                    ADN_Manager cadenaADN = new ADN_Manager();
                    System.out.println(cadenaADN.invertADN(ADN));
                    break;
                case 2:
                    System.out.println("**Trobar la base més repetida "+ADN+"**");
                    System.out.println("");
                    ADN_Manager cadenaADN2 = new ADN_Manager();
                    System.out.println(cadenaADN2.maxLetter(ADN));
                    break;
                case 3:
                    System.out.println("**Trobar la base menys repetida "+ADN+"**");
                    System.out.println("");
                    ADN_Manager cadenaADN3 = new ADN_Manager();
                    System.out.println(cadenaADN3.minLetter(ADN));
                    break;
                case 4:
                    System.out.println("**Fer recompte de bases "+ADN+"**");
                    System.out.println("");
                     ADN_Manager cadenaADN4 = new ADN_Manager();
                     System.out.println("Total de A: "+cadenaADN4.numAdenines(ADN));
                     System.out.println("Total de C: "+cadenaADN4.numCitosines(ADN));
                     System.out.println("Total de G: "+cadenaADN4.numGuanines(ADN));
                     System.out.println("Total de T: "+cadenaADN4.numTimines(ADN));
                     
                    break;
                case 5:
                    break;
            }

        } while (option != 5);

    }

}
