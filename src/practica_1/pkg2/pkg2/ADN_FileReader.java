/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practica_1.pkg2.pkg2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ADN_FileReader {

    
   public  String muestraContenido(String archivo) throws FileNotFoundException, IOException {
        String cadena;
       
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        while ((cadena = b.readLine()) != null) {
            return cadena;
        }
        b.close();
        return null;
    }
   
   
}
